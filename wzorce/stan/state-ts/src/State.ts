import Context from "./Context";

abstract class State {
    context: Context
    name: string

    constructor(name: string) {
        this.name = name
    }

    public setContext = (context: Context) => {
        this.context = context
    }

    public abstract handle(): void
}

export default State