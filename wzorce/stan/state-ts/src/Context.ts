import State from "./State"

class Context {
    private state: State

    constructor(state: State) {
        this.setState(state)
    }

    public setState = (state: State): void => {
        if (!this.state) {
            console.log(`[Context]: Set up state to ${state.name}`)
        } else {
            console.log(`[Context]: Changing state from ${this.state.name} to ${state.name}`)
        }
        
        
        this.state = state
        this.state.setContext(this)
    }

    public handleAction = (): void => {
        this.state.handle()
    }
}

export default Context