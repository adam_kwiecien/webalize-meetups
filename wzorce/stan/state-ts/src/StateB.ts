import State from "./State"

class StateB extends State {
    public handle(): void {
        // Logic...
        
        console.log(`[StateB]: Hi, I'm StateB, I am just doing my job here...`)
    }
}

export default StateB