import Context from "./Context"
import StateA from "./StateA"

const context = new Context(new StateA('StateA'))
context.handleAction() // StateA handling action

setTimeout(() => {
    context.handleAction() // StateB handling action
}, 10000)