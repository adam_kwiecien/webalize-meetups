import State from "./State"
import StateB from "./StateB"

class StateA extends State {
    public handle(): void {
        // Logic...
        
        console.log(`[StateA]: Hi, I'm StateA, I am doing my job and then changing my context state to StateB...`)
        this.context.setState(new StateB('StateB'))
    }
}

export default StateA