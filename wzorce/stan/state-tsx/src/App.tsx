import React, { useEffect, useState } from "react";
import './App.css'

enum ButtonState {
  ADD = 'ADD',
  SUBTRACT = 'SUBTRACT',
  LOG = 'LOG',
  RESET = 'RESET'
}

const App: React.FC = () => {
  const [buttonState, setButtonState] = useState<ButtonState>(ButtonState.ADD)
  const [number, setNumber] = useState<number>(0)

  const handleAction = () => {
    switch(buttonState) {
      case ButtonState.ADD:
        setNumber(number+1)
        break
      case ButtonState.SUBTRACT:
        setNumber(number-1)
        break
      case ButtonState.LOG:
        console.log(number)
        break
      case ButtonState.RESET:
        setNumber(0)
    }
  }

  useEffect(() => {
    console.log('New state: ' + buttonState)
  }, [buttonState])

  return (
    <div className='c-main'>
      <header>
        <h1>Welcome to our <span>React State App</span></h1>
      </header>
      <div>
        <p>Our number is: <span>{number}</span></p>
        <div>
          <h2>Action types: </h2>
          <button 
            onClick={() => setButtonState(ButtonState.ADD)}
            className={`c-btn -action ${buttonState === ButtonState.ADD ? '-active' : ''}`}
          >Increment</button>
          <button 
            onClick={() => setButtonState(ButtonState.SUBTRACT)}
            className={`c-btn -action ${buttonState === ButtonState.SUBTRACT ? '-active' : ''}`}
          >Subtract</button>
          <button 
            onClick={() => setButtonState(ButtonState.LOG)}
            className={`c-btn -action ${buttonState === ButtonState.LOG ? '-active' : ''}`}
          >Log</button>
          <button 
            onClick={() => setButtonState(ButtonState.RESET)}
            className={`c-btn -action ${buttonState === ButtonState.RESET ? '-active' : ''}`}
          >Reset</button>
        </div>
        <div>
          <button 
            className='c-btn -doAction'
            onClick={() => handleAction()}
          >Do action</button>
        </div>
      </div>
    </div>
  )
}

export default App;
