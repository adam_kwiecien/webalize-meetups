<?php

namespace WebalizeMeeting\Pipeline\Nodes;

use WebalizeMeeting\Pipeline\IPipeline;

class Sugar implements IPipeline
{
    private IPipeline $next;
    public const PRICE = 20;

    public function getCost($cost): int
    {
        dump('Sugar');
        if(!isset($this->next)){
            return $cost + self::PRICE;
        }

        return $this->next->getCost($cost + self::PRICE);
    }

    public function setNextNode(IPipeline $next): void
    {
        $this->next = $next;
    }
}