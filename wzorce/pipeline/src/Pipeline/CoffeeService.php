<?php

namespace WebalizeMeeting\Pipeline;

/**
 * @property IPipeline[] $nodes
 */
class CoffeeService
{
    private IPipeline $head;

    /**
     * @param IPipeline[] $addons
     */
    public function setAddons(array $addons): void
    {
        if (empty($addons)) {
            return;
        }

        $addons = array_values($addons);
        $this->head = $addons[0];

        for ($i = 0; $i < count($addons) - 1; $i++) {
            $addons[$i]->setNextNode($addons[$i + 1]);
        }

    }

    public function getCost(int $coffeePrice): int
    {
        if (empty($this->head)) {
            return $coffeePrice;
        }

        return $this->head->getCost($coffeePrice);
    }
}