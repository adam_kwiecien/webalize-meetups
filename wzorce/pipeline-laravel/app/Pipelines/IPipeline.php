<?php

namespace App\Pipelines;

interface IPipeline
{
    public function handle($variable, \Closure $next);
}
