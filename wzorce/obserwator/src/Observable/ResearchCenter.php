<?php

namespace WebalizeMeeting\Observable;

use WebalizeMeeting\Observer\IObserver;

/**
 * @property array $observers
 */
class ResearchCenter implements IObservable
{
    private float $state;
    /**
     * @var IObserver[]
     */
    private array $observers = [];

    public function registerObserver(IObserver $observer): void
    {
        $this->observers[] =  $observer;
    }

    public function deregisterObserver(IObserver $observer): void
    {
        // TODO: Implement deregisterObserver() method.
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this->state);
        }
    }

    public function setState(float $state): void
    {
        $this->state = $state;
        $this->notify();
    }
}