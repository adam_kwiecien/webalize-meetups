<?php

namespace WebalizeTest\Unit;

use WebalizeMeeting\Observable\ResearchCenter;
use WebalizeMeeting\Observer\ResearchStation;
use WebalizeTest\TestCase;

class ExampleTest extends TestCase
{
    public function test_example()
    {
        $centralResearchStation = new ResearchCenter();
        $researchStation1 = new ResearchStation($centralResearchStation, 1);
        $researchStation2 = new ResearchStation($centralResearchStation, 2);
        $researchStation3 = new ResearchStation($centralResearchStation, 3);

        $centralResearchStation->setState(-12);
        $centralResearchStation->setState(-12.3);
    }
}