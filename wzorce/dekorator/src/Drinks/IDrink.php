<?php

namespace WebalizeMeeting\Drinks;

interface IDrink
{
    public function getPrice(): int;
}