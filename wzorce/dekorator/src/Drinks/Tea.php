<?php

namespace WebalizeMeeting\Drinks;

class Tea implements IDrink
{
    public function getPrice(): int
    {
        return 150;
    }
}