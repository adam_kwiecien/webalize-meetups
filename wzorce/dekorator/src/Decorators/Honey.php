<?php

namespace WebalizeMeeting\Decorators;

use WebalizeMeeting\Drinks\IDrink;

class Honey implements IDecorator
{

    private IDrink $drink;

    /**
     * @param IDrink $drink
     */
    public function __construct(IDrink $drink)
    {
        $this->drink = $drink;
    }

    public function getPrice(): int
    {
        return $this->drink->getPrice() + 75;
    }
}