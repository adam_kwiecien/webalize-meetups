<?php

namespace WebalizeMeeting\Decorators;

use WebalizeMeeting\Drinks\IDrink;

interface IDecorator extends IDrink
{
    public function __construct(IDrink $drink);

    public function getPrice(): int;
}