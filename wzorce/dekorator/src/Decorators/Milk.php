<?php

namespace WebalizeMeeting\Decorators;

use WebalizeMeeting\Drinks\IDrink;

class Milk implements IDecorator
{
    private IDrink $drink;

    /**
     * @param IDrink $drink
     */
    public function __construct(IDrink $drink)
    {
        $this->drink = $drink;
    }

    public function getPrice(): int
    {
        dump('milk');
        $price = $this->drink->getPrice();
        dump('milk');
        return $price + 50;
    }
}