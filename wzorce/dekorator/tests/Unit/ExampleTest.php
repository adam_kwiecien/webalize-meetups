<?php

namespace WebalizeTest\Unit;

use WebalizeMeeting\AdamDrink;
use WebalizeTest\TestCase;

class ExampleTest extends TestCase
{
    public function test_example(): void
    {
        $drink = (new AdamDrink())->getDrink();
        dd($drink->getPrice());
    }
}